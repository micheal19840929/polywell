-- Example of a simple game (connect4) made with polywell.

local e = require("polywell")
local lume = require("lume")

local board = { {}, {}, {}, {}, {}, {}, {} }
local colors = {red={255,50,50},yellow={255,238,0}}
local turn = "yellow"

local draw = function()
   for col=1,7 do
      for n,color in ipairs(board[col]) do
         love.graphics.setColor(colors[color])
         local x,y = col * 75, 800 - n*75
         love.graphics.circle("fill", x, y, 30)
      end
   end
end

e.define_mode("connect4", nil, {draw=draw, read_only=true})
e.bind("connect4", "escape", function() e.change_buffer("*console*") end)
e.bind("connect4", "backspace", function()
          for i=1,7 do lume.clear(board[i]) end
end)

-- 1-7 number keys used to place a token
for key=1,7 do
   e.bind("connect4", tostring(key), function()
             local column = board[key]
             if(#column >= 6) then return end
             table.insert(column, turn)
             turn = turn == "red" and "yellow" or "red"
             -- TODO: detect wins
   end)
end

-- nil means it's a special non-file-backed buffer named "*connect4*"
e.open(nil, "*connect4*", "connect4")
