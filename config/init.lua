local editor = require("polywell")

require("config.edit")
require("config.lua_mode")
require("config.console")

-- uncomment this out to enable Emacs key bindings, which conflict with
-- some of the more conventional bindings.
-- require("config.emacs_keys")

editor.open(nil, "*console*")
