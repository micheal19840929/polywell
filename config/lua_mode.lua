-- Mode for -*- lua -*- files
local editor = require("polywell")
local lume = require("polywell.lume")

local keywords = {"and", "break", "do", "else", "elseif", "end", "false",
                  "for", "function", "if", "in", "local", "nil", "not", "or",
                  "repeat", "return", "then", "true", "until", "while", }

keywords.comment_pattern = "[-][-]"

editor.define_mode("lua", "edit",
                   {on_change = lume.fn(editor.colorize, keywords),
                    activate = lume.fn(editor.colorize, keywords)})

editor.add_auto_mode(".*lua", "lua")

editor.bind("lua", "ctrl-alt-r", editor.reload)
editor.bind("lua", "tab", editor.complete)
editor.bind("lua", "alt-/", editor.complete)
