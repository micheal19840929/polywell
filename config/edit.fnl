(local editor (require "polywell"))
(local fennel (require "fennel"))

(fennel.dofile "config/edit-functions.fnl")
(editor.define_mode "edit")

(each [key cmd (pairs {"ctrl-pageup" (partial editor.next_buffer -1)
                       "ctrl-pagedown" editor.next_buffer
                       "return" editor.newline_and_indent

                       "backspace" editor.delete_backwards
                       "delete" editor.delete_forwards

                       "home" editor.beginning_of_line
                       "end" editor.end_of_line
                       "left" editor.backward_char
                       "right" editor.forward_char
                       "up" editor.prev_line
                       "down" editor.next_line
                       "wheelup" editor.prev_line
                       "wheeldown" editor.next_line

                       "ctrl- " editor.mark
                       "ctrl-space" editor.mark

                       "pageup" editor.scroll_up
                       "pagedown" editor.scroll_down

                       "alt-g" editor.go-to-line
                       "ctrl-h" editor.delete_backwards
                       "ctrl-d" editor.delete_forwards
                       "ctrl-k" editor.kill_line
                       "ctrl-a" editor.beginning_of_line
                       "ctrl-e" editor.end_of_line
                       "ctrl-b" editor.backward_char
                       "ctrl-f" editor.forward_char
                       "alt-f" editor.forward_word
                       "alt-b" editor.backward_word
                       "ctrl-p" editor.prev_line
                       "ctrl-n" editor.next_line
                       "alt-" editor.beginning_of_buffer
                       "alt-." editor.end_of_buffer
                       "alt-<" editor.beginning_of_buffer
                       "alt->" editor.end_of_buffer
                       "alt-v" editor.scroll_up
                       "ctrl-v" editor.scroll_down

                       "ctrl- " editor.mark
                       "ctrl-space" editor.mark
                       "ctrl-g" editor.no_mark
                       "alt-w" editor.kill_ring_save
                       "ctrl-w" editor.kill_region
                       "ctrl-y" editor.yank
                       "alt-y" editor.yank_pop

                       "ctrl-backspace" editor.backward_kill_word
                       "alt-d" editor.forward_kill_word
                       "ctrl-alt-r" editor.reload

                       "ctrl-s" editor.search
                       "ctrl-r" (partial editor.search -1)
                       "alt-5" editor.replace

                       "ctrl-x ctrl-f" editor.find-file
                       "ctrl-x b" editor.switch-buffer
                       "ctrl-x ctrl-s" editor.save
                       "ctrl-x k" editor.close
                       "ctrl-x ctrl-c" (fn [] (editor.save) (os.exit 0))

                       "ctrl-x 1" editor.split
                       "ctrl-x 2" (partial editor.split "vertical")
                       "ctrl-x 3" (partial editor.split "horizontal")
                       "ctrl-x 4" (partial editor.split "triple")
                       "ctrl-x o" editor.focus_next})]
  (editor.bind "edit" key cmd))
