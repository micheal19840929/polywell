(local luasocket (require "socket"))

(local send
       (fn [socket command use-separator? args]
         (var str command)
         (when (> (# args) 0)
           (let [sep (if use-separator?
                         " :"
                         (> (# args) 1)
                         " " "")]
             (set str (.. str " " (table.concat args " " 1 (- (# args) 1))
                          sep (. args (# args))))))
         (when debug? (print ">" str))
         (: socket :send (.. str "\r\n"))))

(local push
       (fn [conn command use-separator? args]
         (tset conn.queue conn.bottom [command use-separator? args])
         (set conn.bottom (+ conn.bottom 1))))

(local flush
       (fn [conn]
         (when (not (= conn.top conn.bottom))
           (let [[command separator? args] (. conn.queue conn.top)]
             (tset conn.queue conn.top nil)
             (set conn.top (+ conn.top 1))
             (when (= conn.top conn.bottom)
               (set (conn.top conn.bottom) (values 1 1)))
             (send conn.socket command use-separator? args)))))

(local tokenize
       (fn [line]
         (let [(_ e1 prefix)  (: line :find "^:(%S+)")
               (_ e2 command) (: line :find "(%S+)" (if e1 (+ e1 1) 1))
               (_ _ rest)     (: line :find "%s+(.*)" (if e2 (+ e2 1) 1))]
           (values prefix command rest))))

(local connect
       (fn [host port]
         (let [conn {:queue [] :top 1 :bottom 1 :socket (luasocket.tcp)}]
           (assert (: conn.socket :connect host (or port 6667)))
           (: conn.socket :settimeout 0.1)
           conn)))

(let [irc {:connect connect :tokenize tokenize :flush flush}]
  ;; define functions for IRC operations
  (each [name separator? (pairs {:nick false :user true :join false :kick true
                                 :privmsg true :ping false :pong false})]
    (tset irc name
          (fn [conn ...]
            (push conn (: name :upper) separator? [...]))))
  irc)
