(local editor (require "polywell"))
(let [keywords ["let" "set" "tset" "fn" "lambda" "λ" "require" "if" "when"
                "do" "block" "true" "false" "nil" "values" "pack" "for" "each"
                "local" "partial" "and" "or" "not" "special" "macro"]]
  (set keywords.comment_pattern ";")
  (editor.define_mode "fennel" "edit"
                      {:on_change (partial editor.colorize keywords)
                       :activate (partial editor.colorize keywords)})
  (editor.add_auto_mode "*.fnl" "fennel"))
