(local editor (require "polywell"))
(local fennel (require "fennel"))
(local lume (require "polywell.lume"))
(local serpent (require "serpent"))

(local print-eval
       (fn [input]
         (editor.print (serpent.block (fennel.eval input)
                                      {:maxlevel 8 :maxnum 64 :nocode true}))))

(local activate (fn []
                  (editor.set_prompt ">> ")
                  (editor.print_prompt)))

(local eval (fn []
              (let [input (editor.get_input)]
                (editor.history_push input)
                (editor.end_of_line)
                (editor.newline)
                (editor.no_mark)
                (editor.with_output_to "*repl*" (fn [] (print-eval input)))
                (editor.print_prompt))))

(require "config.console")
(editor.define_mode "repl" "console" {:activate activate})
(editor.bind "repl" "return" eval)
