(local editor (require "polywell"))

(require "config.edit")
(require "config.fennel-mode")
(require "config.repl")
(require "config.irc")

(editor.open nil "*repl*" "repl")
(editor.change_buffer "*repl*")
