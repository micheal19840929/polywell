(local irclib (require "config.irclib"))
(local editor (require "polywell"))
(local lume (require "polywell.lume"))

(require "config.console")

;; make it global so it's reload-friendly
(global irc-state (or irc-state {"debug?" true :nick "texnomancy"}))

(fn incoming [prefix rest]
  (let [channel (: rest :match "(%S+)")
        msg (: rest :match ":(.*)")
        sender (: prefix :match "(%S+)!")
        channel (if (: channel :find "^#") channel sender)
        output (: "%s: %s" :format sender msg)]
    (if (. irc-state.channels-open channel)
        (editor.with_output_to channel (fn [] (editor.print output)))
        (editor.print output))))

(local commands {:ping (fn [prefix rest] (irclib.pong irc-state.client rest))
                 :376 (fn [prefix rest] (set irc-state.ready? true))
                 :privmsg incoming})

(fn update []
  (coroutine.yield)
  (when irc-state.client
    (irclib.flush irc-state.client)
    (let [(line err) (: irc-state.client.socket :receive "*l")]
      (if (= err "closed")
          (set irc-state.client nil)
          (when line
            (when irc-state.debug? (print "<" line))
            (let [(prefix command rest) (irclib.tokenize line)
                  command (and command (: command :lower))
                  handler (. commands command)]
              (when handler
                (let [(ok err) (pcall handler prefix rest)]
                  (when (not ok)
                    (print "Error:" err)))))))))
  (update))

(local send
       (fn []
         (let [input (editor.get_input)
               channel (editor.current_buffer_path)
               out (.. nick ": " input)]
           (editor.history_push input)
           (editor.end_of_line)
           (editor.newline)
           (editor.no_mark)
           (when irc-state.debug? (print ">" input))
           (irclib.privmsg irc-state.client channel (.. ":" input))
           (editor.with_output_to channel (fn [] (editor.print out)))
           (editor.print_prompt))))

(local join
       (fn []
         (when irc-state.ready?
           (let [(channel cancel) (editor.read_input "Channel: ")]
             (when (and channel (not cancel))
               (irclib.join irc-state.client channel)
               (tset irc-state.channels-open channel true)
               (editor.open nil channel "irc")
               (editor.change_buffer channel))))))

(editor.define_mode "irc-server" "edit" {:read_only true})
;; not ideal, but it works
(editor.bind "irc-server" "return" (coroutine.wrap join))

(editor.define_mode "irc" "console" {:activate (fn []
                                                 (editor.set_prompt "=> ")
                                                 (editor.print_prompt))})
(editor.bind "irc" "return" send)

(local irc-connect
       (fn []
         (let [;; (server cancel) (editor.read_input "Server: ")
               server "chat.freenode.net"
               server-buffer (: "*irc %s*" :format server)]
           (when (and irc-state.client irc-state.client.socket)
             (: irc-state.client.socket :close))
           (when (and (not cancel) server)
             ;; (set irc-state.nick (editor.read_input "Nick: "))
             (set irc-state.ready? false)
             (set irc-state.client (irclib.connect server 6667))
             (set irc-state.channels-open {})
             (irclib.nick irc-state.client irc-state.nick)
             (irclib.user irc-state.client (or (os.getenv "USER") "someone")
                          "0" "*" "Polywell User")
             (editor.open nil server-buffer "irc-server")
             (editor.start update)
             (editor.change_buffer server-buffer)))))

;; entry-point function
(global irc (coroutine.wrap irc-connect))
