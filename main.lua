local fennel = require("fennel")
table.insert(package.loaders, fennel.make_searcher({correlate=true}))

local polywell = require("polywell")
local fs_for = require("polywell.fs")
local lume = require("polywell.lume")

love.keyreleased = polywell.keyreleased
love.keypressed = polywell.keypressed
love.textinput = polywell.textinput
love.wheelmoved = polywell.wheelmoved
love.mousepressed = polywell.mousepressed
love.mousereleased = polywell.mousereleased
love.mousemoved = polywell.mousemoved
love.mousefocus = polywell.mousefocus

local refresh = function()
   love.graphics.clear(love.graphics.getBackgroundColor())
   love.graphics.origin()
   polywell.draw()
   love.graphics.present()
end

-- Use this if you never have any code that needs to run other than reacting
-- to user input. More efficient.
-- love.run = function()
--    love.load()
--    while true do
--       love.event.pump()
--       local needs_refresh = false
--       for name, a,b,c,d,e,f in love.event.poll() do
--          if(type(love[name]) == "function") then
--             love[name](a,b,c,d,e,f)
--             needs_refresh = true
--          elseif(name == "quit") then
--             os.exit()
--          end
--       end
--       for _,c in pairs(polywell.coroutines) do
--          local ok, val = coroutine.resume(c)
--          if(ok and val) then needs_refresh = true
--          elseif(not ok) then print(val) end
--       end
--       for i,c in lume.ripairs(polywell.coroutines) do
--          if(coroutine.status(c) == "dead") then
--             table.remove(polywell.coroutines, i)
--          end
--       end
--       if(needs_refresh) then refresh() end
--       love.timer.sleep(0.05)
--    end
-- end

-- Use this if you want to use love's built-in event loop. More flexible.
love.update = function()
   for _,c in pairs(polywell.coroutines) do
      local ok, val = coroutine.resume(c)
      if(ok and val) then needs_refresh = true
      elseif(not ok) then print(val) end
   end
   for i,c in lume.ripairs(polywell.coroutines) do
      if(coroutine.status(c) == "dead") then
         table.remove(polywell.coroutines, i)
      end
   end
end
love.draw = polywell.draw

love.load = function()
   local multiprint = function(x) print(x) polywell.print(x) end
   love.graphics.setFont(love.graphics.newFont("inconsolata.ttf", 14))
   love.keyboard.setTextInput(true)
   love.keyboard.setKeyRepeat(true)

   polywell.fs = polywell.fs or fs_for(os.getenv("PWD") or ".")
   -- You can use the init written in lua:
   -- dofile("config/init.lua")
   -- Or the init code written in Fennel:
   fennel.dofile("config/init.fnl")
end
