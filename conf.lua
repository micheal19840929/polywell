-- love2d configuration
love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "Polywell", "polywell"
   t.modules.joystick, t.modules.physics = false, false
   t.modules.sound, t.modules.audio = false, false
   t.window.resizable = true
end
