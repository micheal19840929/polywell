LUA=polywell/*.lua polywell/frontend/*.lua  config/*.lua

check:
	luacheck --no-color --globals love -- $(LUA)

count:
	cloc $(LUA)
