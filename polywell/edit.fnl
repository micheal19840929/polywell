(local lume (require "polywell.lume"))
(local utils (require "polywell.utils"))
(local utf8 (require "polywell.utf8"))

{:find-file
 (fn []
   (let [callback (λ [input cancel]
                    (when (and (not cancel)
                               (= (type (. editor.fs input)) "string"))
                      (editor.open editor.fs input)))

         completer          ; offer live feedback as you type
         (λ [input]
           (let [separator (and (getmetatable editor.fs)
                                (. (getmetatable editor.fs)
                                   "__separator"))
                 completions (utils.completions_for input editor.fs
                                                    separator)
                 ;; different types should be distinct
                 decorate (λ [path]
                            (let [x (. editor.fs path)]
                              (if (= (type x) "table")
                                  (.. path separator)
                                  (= (type x) "string")
                                  path
                                  :else false)))]
             ;; if it's not a table/string, we shouldn't see it
             (lume.sort (lume.filter (lume.map completions decorate)
                                     (λ [s1 s2] (< (# s1) (# s2)))))))]
     (editor.read_line "Open: " callback {:completer completer})))
 :search
 (fn [original-dir]
   (let [lines (editor.get_lines)
         (point point-line) (editor.point)
         continue-from point-line
         path (editor.current_buffer_path)
         on-change (λ [find-next new-dir]
                     (let [input (editor.get_input)
                           direction (or new-dir original-dir 1)
                           start (if find-next
                                     continue-from
                                     point-line)]
                       (var i start)
                       (var match nil)
                       (while (and (not (= input "")) (not match)
                                   (> line 0) (<= line (# lines)))
                         (set match (string.find (. lines i) input))
                         (when match
                           (set! continue-from (+ i direction))
                           (editor.go_to i (- match 1) path))
                         (set i (+ i direction)))))
         callback (λ [_ cancel]
                    (when cancel
                      (editor.go_to point-line point)))
         cancel (partial editor.exit_minibuffer true)]
     (editor.read_line "Search: " callback
                       {:on_change on-change
                        :bind {"ctrl-g" cancel
                               "ctrl-n" cancel
                               "ctrl-p" cancel
                               "ctrl-f" (partial on-change true)
                               "ctrl-s" (partial on-change true 1)
                               "ctrl-r" (partial on-change true -1)}})))

 :replace
 (fn []
   (let [lines (editor.get_lines)
         (point point-line) (editor.point)
         path (editor.current_buffer_path)
         actually-replace (fn [replace i with replacer y cancel]
                            (when (and (not cancel)
                                       (or (= y "")
                                           (= y "y")
                                           (= (string.lower y) "yes")))
                              (let [new (string.gsub (. lines i)
                                                     replace with)]
                                (editor.set_line new i path)
                                (replacer with false (+ i 1)))))
         replacer
         (fn replacer [replace with cancel continue-from]
           (if cancel
               (editor.go_to point-line point)
               (do
                 (var match-point nil)
                 (var i (or continue-from point-line))
                 (while (and (not match-point)
                             (< i (# lines)))
                   (let [match-point (string.find (. lines i) replace)]
                     (when match-point
                       (editor.go_to i (- match-point 1) path)
                       (editor.read_line "Replace? [Y/n]"
                                         (partial actually-replace
                                                  replace i with replacer)))
                     (set i (+ i 1)))))))]
     (editor.read-line "Replace: "
                       (fn [replace-text cancel]
                         (when (not cancel)
                           (editor.read_line (.. "Replace " replace-text
                                                 " with: ")
                                             (partial replacer
                                                      replace-text)))))))
 :switch-buffer
 (fn []
   (let [last-buffer (or (editor.last_buffer) "*repl*")
         callback (fn [b cancel]
                    (when (not cancel)
                      (editor.change_buffer (if (= b "")
                                                last-buffer
                                                b))))
         completer (fn [input]
                     (utils.completions_for input (editor.buffer_names)))]
     (editor.read_line (.. "Switch to buffer (default: " last-buffer "): ")
                       callback {:completer completer})))
 :reload
 (fn []
   (each [_ b (pairs (editor.buffer_names))]
     (editor.save nil b))
   ((. (require "fennel") "dofile") "config/init.fnl")
   (editor.print "Successfully reloaded config."))
 :complete
 (fn []
   (let [(_ point-line) (editor.point)
         line (editor.get_line point-line)
         entered (or (lume.last (lume.array (string.gmatch line
                                                           "[._%a0-9]+")))
                     "")]
     (when (>= (# entered) 1)
       (let [completions (utils.completions_for entered _G ".")]
         (if (= (# completions) 1)
             (editor.textinput (utf8.sub (. completions 1)
                                         (+ (# entered) 1)) true)
             (> (# completions) 0)
             (let [common (utils.longest_common_prefix completions)]
               (if (= common entered)
                   (editor.echo (table.concat completions " "))
                   (editor.textinput (utf8.sub common (+ (# entered) 1))
                                     true))))))))
 :go-to-line
 (fn []
   (editor.read_line "Go to line: "
                     (fn [l cancel]
                       (when (not cancel)
                         (editor.go_to (tonumber l))))))}
