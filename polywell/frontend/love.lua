-- This file contains all the love2d-specific code; in theory replacing this
-- could allow polywell to run on another lua-based framework.

local row_height, scroll_rows, em, w, h
local padding, buffer_padding, scroll_point = 10, 0, 0.8

local render_line = function(ln2, y)
   if(ln2 == "\f\n" or ln2 == "\f") then
      love.graphics.line(0, y + 0.5 * row_height, w, y + 0.5 * row_height)
   else
      love.graphics.print(ln2, buffer_padding, y)
   end
end

local render_buffer = function(b, colors, bh, focused)
   local display_rows = math.floor(bh / row_height)
   local edge = math.ceil(display_rows * scroll_point)
   local offset = (b.point_line < edge and 0) or (b.point_line - edge)
   if(focused or not scroll_rows) then scroll_rows = display_rows end
   for i,line in ipairs(b.props.render_lines or b.lines) do
      if(i >= offset) then
         local row_y = row_height * (i - offset)
         if(row_y >= h - row_height) then break end

         if(i == b.mark_line) then -- mark
            love.graphics.setColor(colors.mark)
            love.graphics.rectangle("line", b.mark*em, row_y, em, row_height)
         end
         if(i == b.point_line) then -- point and point line
            love.graphics.setColor(colors.point_line)
            love.graphics.rectangle("fill", 0, row_y, w, row_height)
            love.graphics.setColor(colors.point)
            love.graphics.rectangle(focused and "fill" or "line",
                                    buffer_padding+b.point*em, row_y,
                                    em, row_height)
         end

         if(b.props.render_lines) then -- fancy colors get ANDed w base colors
            if(love._version_major == 0) then
               love.graphics.setColor(255, 255, 255)
            else
               love.graphics.setColor(1, 1, 1)
            end
         else
            love.graphics.setColor(colors.text)
         end
         render_line(line, row_y)
      end
   end
end

local draw_scroll_bar = function(b, colors)
   -- this only gives you an estimate since it uses the amount of
   -- lines entered rather than the lines drawn, but close enough

   -- height is percentage of the possible lines
   local bar_height = math.min(100, (scroll_rows * 100) / #b.lines)
   -- convert to pixels (percentage of screen height, minus 10px padding)
   local bar_height_pixels = (bar_height * (h - 10)) / 100

   local sx = w - 5
   love.graphics.setColor(colors.scroll_bar)
   -- Handle the case where there are less actual lines than display rows
   if bar_height_pixels >= h - 10 then
      love.graphics.line(sx, 5, sx, h - 5)
   else
      -- now determine location on the screen by taking the offset in
      -- history and converting it first to a percentage of total
      -- lines and then a pixel offset on the screen
      local bar_end = (b.point_line * 100) / #b.lines
      bar_end = ((h - 10) * bar_end) / 100

      local bar_begin = bar_end - bar_height_pixels
      -- Handle overflows
      if bar_begin < 5 then
         love.graphics.line(sx, 5, sx, bar_height_pixels)
      elseif bar_end > h - 5 then
         love.graphics.line(sx, h - 5 - bar_height_pixels, sx, h - 5)
      else
         love.graphics.line(sx, bar_begin, sx, bar_end)
      end
   end
end

local draw = function(b, buffers_where, echo_message, colors, get_prop)
   row_height = love.graphics.getFont():getHeight()
   em = love.graphics.getFont():getWidth('a')
   w, h = love.window.getMode()

   -- Draw background
   if(#buffers_where > 0) then
      love.graphics.setColor(colors.background)
      love.graphics.rectangle("fill", 0, 0, w, h)
   end

   for pos,buf in pairs(buffers_where) do
      local x,y,bw,bh = unpack(pos)
      love.graphics.push()
      love.graphics.translate(x, y)
      love.graphics.setScissor(x, y, bw, bh)
      if(get_prop("draw", nil, buf)) then
         get_prop("draw", nil, buf)()
      else
         render_buffer(buf, colors, bh, buf == b)
      end
      love.graphics.pop()
      love.graphics.setScissor()
   end

   love.graphics.setColor(colors.minibuffer_bg)
   love.graphics.rectangle("fill", 0, h - row_height, w, row_height)
   love.graphics.setColor(colors.minibuffer_fg)

   if(b.path == "minibuffer") then
      love.graphics.print(b:render(), padding, h - row_height)
      love.graphics.setColor(colors.point)
      love.graphics.rectangle("fill", padding+b.point*em,
                              h - row_height, em, row_height)
   elseif(echo_message) then
      love.graphics.print(echo_message, padding, h - row_height)
   else
      love.graphics.print(b:modeline(), padding, h - row_height)
   end

   if(b.path ~= "minibuffer") then draw_scroll_bar(b, colors) end
end

return {
   write = love.filesystem.write,
   read = love.filesystem.read,

   is_key_down = love.keyboard.isScancodeDown or love.keyboard.isDown,
   set_cursor = function(cursor)
      if(cursor == nil) then return end
      if(type(cursor) == "table") then
         return love.mouse.newCursor("assets/" .. cursor[1] .. ".png",
                                     cursor[2], cursor[3])
      else
         return love.mouse.getSystemCursor(cursor)
      end
   end,

   get_clipboard = function()
      return love.window and love.system.getClipboardText()
   end,
   set_clipboard = function(contents)
      if love.window then love.system.setClipboardText(contents) end
   end,

   get_wh = function()
      local _,_,sw,sh = love.graphics.getScissor()
      if(not sw and not sh) then sw,sh = love.window.getMode() end
      return sw, sh
   end,
   fullscreen = function()
      local dw,dh = love.window.getDesktopDimensions()
      love.window.setMode(dw, dh, {fullscreen=true, fullscreentype="desktop",
                                   resizable=false})
   end,

   normalize_color = function(c)
      -- support for old-style 255-based colors in love 0.x
      if(love._version_major > 0) then
         local new = {}
         for i,n in pairs(c) do new[i] = n/255 end
         return new
      else
         return c
      end
   end,

   draw = draw,
}
