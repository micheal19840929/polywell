(local old (require :polywell.old))
(local edit (require :polywell.edit))

(each [k v (pairs edit)]
  (tset old k v))

old
